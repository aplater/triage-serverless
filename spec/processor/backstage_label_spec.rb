# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/backstage_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BackstageLabel do
  subject { described_class.new(event) }

  let(:event) do
    instance_double('Triage::Event',
      from_gitlab_org?: true,
      added_label_names: added_label_names,
      user: { 'username' => Triage::Event::ENG_PROD_TEAM_USERNAMES.first },
      noteable_path: '/foo')
  end

  describe '#process' do
    shared_examples 'no message posting' do
      it 'does not call #post_deprecation_message' do
        expect(subject).not_to receive(:post_deprecation_message)

        subject.process
      end
    end

    shared_examples 'message posting' do |label|
      it 'posts a deprecation message' do
        body = <<~MARKDOWN.chomp
          Hey @#{Triage::Event::ENG_PROD_TEAM_USERNAMES.first}, ~"#{label}" is being deprecated in favor of ~"feature::addition", ~"feature::maintenance", ~"tooling::pipelines", and ~"tooling::workflow" to improve the identification of these type of changes.
          Please see https://about.gitlab.com/handbook/engineering/management/throughput/#implementation for further guidance.
          /unlabel ~"#{label}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when there is no backstage label' do
      let(:added_label_names) { ['foo'] }

      it_behaves_like 'no message posting'
    end

    described_class::BACKSTAGE_LABELS.each do |label|
      context 'when there is the backstage label' do
        let(:added_label_names) { [label] }

        context 'when event project is not under gitlab-org' do
          it 'does not call #post_deprecation_message' do
            expect(event).to receive(:from_gitlab_org?).and_return(false)
            expect(subject).not_to receive(:post_deprecation_message)

            subject.process
          end
        end

        it_behaves_like 'message posting', label
      end
    end
  end
end
