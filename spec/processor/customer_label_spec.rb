# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/customer_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CustomerLabel do
  subject { described_class.new(event) }

  let(:event) { instance_double('Triage::Event', new_comment: new_comment) }

  describe '#process' do
    context 'when new comment has gitlab.zendesk.com' do
      let(:new_comment) { 'gitlab.zendesk.com' }

      it 'calls add_customer_label' do
        expect(subject).to receive(:add_customer_label)

        subject.process
      end
    end

    context 'when new comment has gitlab.my.salesforce.com' do
      let(:new_comment) { 'gitlab.my.salesforce.com' }

      it 'calls add_customer_label' do
        expect(subject).to receive(:add_customer_label)

        subject.process
      end
    end

    context 'when new comment has gitlab.com' do
      let(:new_comment) { 'gitlab.com' }

      it 'does not call add_customer_label' do
        expect(subject).not_to receive(:add_customer_label)

        subject.process
      end
    end

    context 'when new comment is empty' do
      let(:new_comment) { '' }

      it 'does not call add_customer_label' do
        expect(subject).not_to receive(:add_customer_label)

        subject.process
      end
    end

    context 'when there is a customer link' do
      let(:event) do
        Triage::Event.build(
          JSON.parse(File.read("#{__dir__}/../fixture/note_on_issue.json"))
        )
      end

      before do
        allow(subject).to receive(:has_customer_link?).and_return(true)
      end

      it 'uses noteable_path to post a comment to add a customer label' do
        expect_comment_request(event: event, body: '/label ~customer') do
          subject.process
        end
      end
    end
  end
end
