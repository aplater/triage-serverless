# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/deprecated_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::DeprecatedLabel do
  subject { described_class.new(event) }

  let(:event) do
    instance_double('Triage::Event',
           from_gitlab_org?: true,
           added_label_names: added_label_names,
           user: { 'username' => Triage::Event::ENG_PROD_TEAM_USERNAMES.first },
           noteable_path: '/foo')
  end

  describe '#process' do
    shared_examples 'no message posting' do
      it 'does not call #post_replacement_message' do
        expect(subject).not_to receive(:post_replacement_message)

        subject.process
      end
    end

    shared_examples 'message posting' do |deprecated_label, new_label|
      it 'posts a replacement message' do
        body = <<~MARKDOWN.chomp
          Hey @#{Triage::Event::ENG_PROD_TEAM_USERNAMES.first}, please use ~"#{new_label}" as ~"#{deprecated_label}" has been deprecated.
          /unlabel ~"#{deprecated_label}"
          /label ~"#{new_label}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when there is no deprecated label' do
      let(:added_label_names) { ['foo'] }

      it_behaves_like 'no message posting'
    end

    context 'when event project is not under gitlab-org' do
      let(:added_label_names) { ['Manage [DEPRECATED]'] }

      it 'does not call #post_replacement_message' do
        expect(event).to receive(:from_gitlab_org?).and_return(false)
        expect(subject).not_to receive(:post_replacement_message)

        subject.process
      end
    end

    described_class::DEPRECATED_LABEL_REPLACEMENT.each do |deprecated_label, new_label|
      context 'when there is a deprecated label' do
        let(:added_label_names) { [deprecated_label] }

        it_behaves_like 'message posting', deprecated_label, new_label
      end
    end

    context 'multiple deprecated labels added' do
      let(:deprecated_labels) { ['Manage [DEPRECATED]', 'Create [DEPRECATED]'] }
      let(:added_label_names) { deprecated_labels + ['valid label'] }
      let(:replacement_labels) { ['devops::manage', 'devops::create'] }

      it 'posts a replacement message for deprecated labels only' do
        body = <<~MARKDOWN.chomp
          Hey @#{Triage::Event::ENG_PROD_TEAM_USERNAMES.first}, please use #{replacement_labels.map { |l| %Q(~"#{l}") }.join(' ')} as #{deprecated_labels.map { |l| %Q(~"#{l}") }.join(' ')} has been deprecated.
          /unlabel #{deprecated_labels.map { |l| %Q(~"#{l}") }.join(' ') }
          /label #{replacement_labels.map { |l| %Q(~"#{l}") }.join(' ')}
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
