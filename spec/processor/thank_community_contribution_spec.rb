# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/thank_community_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::ThankCommunityContribution do
  subject { described_class.new(event) }

  let(:event_author) { Triage::Event::ENG_PROD_TEAM_USERNAMES.first }
  let(:event) do
    instance_double('Triage::Event',
      from_gitlab_org?: true,
      wider_community_author?: true,
      merge_request?: true,
      new_entity?: true,
      user: { 'username' => event_author },
      noteable_path: '/foo')
  end

  describe '#process' do
    shared_examples 'no message posting' do
      it 'does not call #post_thank_you_message' do
        expect(subject).not_to receive(:post_thank_you_message)

        subject.process
      end
    end

    shared_examples 'message posting' do
      it 'posts a deprecation message' do
        body = <<~MARKDOWN.chomp
          :wave: @#{event_author}

          Thank you for your contributions to GitLab. We believe that [everyone can contribute](https://about.gitlab.com/company/strategy/#everyone-can-contribute) and contributions like yours are what make GitLab great!

          Our [Merge Request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach) will ensure your contribution is reviewed in a timely manner.

          If after a few days, there's no message from a GitLab team member, feel free to ping `@gitlab-org/coaches`.

          These resources may help you to move your Merge Request to the next steps:

          - [Contributing to GitLab](https://about.gitlab.com/community/contribute/)
          - [How to get help](https://about.gitlab.com/community/contribute/#getting-help)
          - [Ping a Merge request coach](https://about.gitlab.com/company/team/?department=merge-request-coach)

          /label ~"Community contribution"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not for a merge request' do
      before do
        allow(event).to receive(:merge_request?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not for a new entity' do
      before do
        allow(event).to receive(:new_entity?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_community_author?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is for a new merge request opened by a wider community author under the gitlab-org group' do
      it_behaves_like 'message posting'
    end
  end
end
