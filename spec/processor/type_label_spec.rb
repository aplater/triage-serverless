# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/type_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::TypeLabel do
  subject { described_class.new(event) }

  let(:added_label_names) { [] }
  let(:label_names) { added_label_names }
  let(:event) do
    instance_double('Triage::Event',
      from_gitlab_org?: true,
      label_names: label_names,
      added_label_names: added_label_names,
      noteable_path: '/foo')
  end

  describe '#process' do
    shared_examples 'no message posting' do
      it 'does not call #set_type_label' do
        expect(subject).not_to receive(:set_type_label)

        subject.process
      end
    end

    shared_examples 'message posting' do |type_label|
      it "adds the relevant type label" do
        type_labels_to_remove = described_class::TYPE_LABELS.reject { |label| label == type_label }
        body = <<~MARKDOWN.chomp
          /label ~"#{type_label}"
          /unlabel #{type_labels_to_remove.map { |l| %Q(~"#{l}") }.join(' ')}
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when there is no sub-type label' do
      it_behaves_like 'no message posting'
    end

    context 'when there is a sub-type label' do
      context 'when there is no type label' do
        described_class::TYPE_LABELS.each do |type_label|
          context 'when event project is not under gitlab-org' do
            before do
              allow(event).to receive(:from_gitlab_org?).and_return(false)
            end

            it_behaves_like 'no message posting'
          end

          context "when there is a sub-type label of #{type_label}" do
            let(:added_label_names) { %W[#{type_label}::subtype] }

            it_behaves_like 'message posting', type_label
          end
        end
      end

      context 'when there is an existing type label corresponding to the type label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when there is a sub-type label of #{type_label}" do
            let(:added_label_names) { %W[#{type_label}::subtype] }
            let(:label_names) { [type_label] + added_label_names }

            it_behaves_like 'no message posting'
          end
        end
      end

      context 'when there is a new type label corresponding to the type label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when there is a sub-type label of #{type_label}" do
            let(:added_label_names) { [type_label, "#{type_label}::subtype"] }

            it_behaves_like 'no message posting'
          end
        end
      end

      context 'when there is an existing type label not corresponding to the type label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when there is a sub-type label of #{type_label}" do
            let(:added_label_names) { %W[#{type_label}::subtype] }
            let(:label_names) { [described_class::TYPE_LABELS[described_class::TYPE_LABELS.index(type_label) - 1]] + added_label_names }

            it_behaves_like 'message posting', type_label
          end
        end
      end

      context 'when there is a new type label not corresponding to the type label' do
        described_class::TYPE_LABELS.each do |type_label|
          context "when there is a sub-type label of #{type_label}" do
            let(:added_label_names) { [described_class::TYPE_LABELS[described_class::TYPE_LABELS.index(type_label) - 1], "#{type_label}::subtype"] }

            it_behaves_like 'message posting', type_label
          end
        end
      end
    end
  end
end
