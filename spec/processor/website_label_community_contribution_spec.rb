# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/website_label_community_contribution'
require_relative '../../triage/triage/event'

RSpec.describe Triage::WebsiteLabelCommunityContribution do
  subject { described_class.new(event) }

  let(:event_author) { Triage::Event::ENG_PROD_TEAM_USERNAMES.first }
  let(:event) do
    instance_double('Triage::Event',
      from_gitlab_com?: true,
      wider_gitlab_com_community_author?: true,
      with_project_id?: true,
      merge_request?: true,
      new_entity?: true,
      user: { 'username' => event_author },
      noteable_path: '/foo')
  end

  describe '#process' do
    shared_examples 'no message posting' do
      it 'does not call #post_thank_you_message' do
        expect(subject).not_to receive(:post_thank_you_message)

        subject.process
      end
    end

    shared_examples 'message posting' do |label|
      it 'posts a thank you message' do
        body = <<~MARKDOWN.chomp
          Hi @#{event_author},

          Thanks so much for your contribution to the GitLab website! :heart:

          I'll notify the Website team about your Merge Request and they will get back to you as soon as they can.
          If you don't hear from someone in a reasonable amount of time, please ping us again in a comment and mention @gl-website.

          /label ~"Community contribution"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event project is not under gitlab-com' do
      before do
        allow(event).to receive(:from_gitlab_com?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event project is not under www-gitlab-com' do
      before do
        allow(event).to receive(:with_project_id?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not for a merge request' do
      before do
        allow(event).to receive(:merge_request?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not for a new entity' do
      before do
        allow(event).to receive(:new_entity?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is not from a wider community author' do
      before do
        allow(event).to receive(:wider_gitlab_com_community_author?).and_return(false)
      end

      it_behaves_like 'no message posting'
    end

    context 'when event is for a new merge request opened by a wider community author in the www-gitlab-com project' do
      it_behaves_like 'message posting'
    end
  end
end
