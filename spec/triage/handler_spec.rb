# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/handler'

RSpec.describe Triage::Handler do
  subject { described_class.new(context, event) }

  let(:context) { { 'HTTP_X_GITLAB_TOKEN' => input_token } }
  let(:event) { {} }
  let(:input_token) { 'input_token' }

  describe '#initialize' do
    context 'when webhook_token is empty' do
      before do
        stub_env('GITLAB_WEBHOOK_TOKEN' => '')
      end

      it 'raises ArgumentError' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#authenticated?' do
    context 'when webhook_token has something' do
      before do
        stub_env('GITLAB_WEBHOOK_TOKEN' => actual_token)

        expect(subject).to receive(:secure_compare).and_call_original
      end

      context 'when actual token is the same' do
        let(:actual_token) { input_token }

        it 'returns true' do
          expect(subject).to be_authenticated
        end
      end

      context 'when actual token is different' do
        let(:actual_token) { 'fake' }

        it 'returns false' do
          expect(subject).not_to be_authenticated
        end
      end
    end
  end
end
