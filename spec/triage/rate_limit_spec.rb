# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/triager'
require_relative '../../triage/triage/rate_limit'

RSpec.describe Triage::RateLimit do
  let(:mock) { double('mock', call: true) }
  let(:event) { instance_double('Triage::Event', user: { 'id' => 1 }) }

  let(:processor) do
    Class.new(Triage::Triager) do
      include Triage::RateLimit

      def initialize(event, mock)
        super(event)
        @mock = mock
      end

      def cache_key
        "test-key-#{event.user['id']}"
      end

      def process
        @mock.call
      end
    end
  end

  subject { processor.new(event, mock) }

  describe 'RATE_LIMIT' do
    it { expect(described_class::RATE_LIMIT).to eq(60) }
  end

  describe 'RATE_LIMIT_PERIOD' do
    it { expect(described_class::RATE_LIMIT_PERIOD).to eq(3600) }
  end

  describe 'rate limiting', :clean_cache do
    it 'records the number of events sent by author and resets the counter after 1 hour' do
      mock2 = double('mock', call: true)
      event2 = instance_double('Triage::Event', user: { 'id' => 2 })
      subject2 = processor.new(event2, mock2)

      expect(Triage.cache.get(subject.cache_key)).to be_nil
      expect(Triage.cache.get(subject2.cache_key)).to be_nil

      subject.triage
      expect(mock).to have_received(:call).once

      expect(Triage.cache.get(subject.cache_key)).to eq(1)

      Timecop.travel(Time.now + described_class::RATE_LIMIT_PERIOD / 2)

      subject.triage
      subject2.triage

      expect(mock).to have_received(:call).twice
      expect(mock2).to have_received(:call).once

      expect(Triage.cache.get(subject.cache_key)).to eq(2)
      expect(Triage.cache.get(subject2.cache_key)).to eq(1)

      Timecop.travel(Time.now + (described_class::RATE_LIMIT_PERIOD / 2) + 1)

      expect(Triage.cache.get(subject.cache_key)).to be_nil
      expect(Triage.cache.get(subject2.cache_key)).to eq(1)

      Timecop.travel(Time.now + (described_class::RATE_LIMIT_PERIOD / 2) + 1)

      expect(Triage.cache.get(subject2.cache_key)).to be_nil
    end

    it 'does not process event if user exceeds the allowed limit' do
      stub_const("#{described_class}::RATE_LIMIT", 1)

      subject.triage
      subject.triage

      expect(mock).to have_received(:call).at_most(:once)
    end
  end
end
