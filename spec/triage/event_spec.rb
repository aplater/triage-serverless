# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'

RSpec.describe Triage::Event do
  subject { described_class.build(event) }

  let(:event) do
    JSON.parse(File.read("#{__dir__}/../fixture/#{fixture_path}"))
  end

  describe '.build' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::IssueEvent)
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::MergeRequestEvent)
      end
    end

    context 'when the event is a note on an issue' do
      let(:fixture_path) { 'note_on_issue.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::NoteEvent)
      end
    end

    context 'when the event is a note on a merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::NoteEvent)
      end
    end
  end

  describe '#issue?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns true' do
        expect(subject).to be_issue
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns false' do
        expect(subject).not_to be_issue
      end
    end
  end

  describe '#merge_request?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_merge_request
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns true' do
        expect(subject).to be_merge_request
      end
    end
  end

  describe '#note?' do
    context 'when the event is not a note' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_note
      end
    end

    context 'when the event is a new note on an issue' do
      let(:fixture_path) { 'note_on_issue.json' }

      it 'returns true' do
        expect(subject).to be_note
      end
    end

    context 'when the event is a new note on a MR' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns true' do
        expect(subject).to be_note
      end
    end
  end

  describe '#user' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns a hash of the user' do
        expect(subject.user).to eq(event['user'])
      end
    end
  end

  describe '#author_id' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.author_id' do
        expect(subject.author_id).to eq(event['object_attributes']['author_id'])
      end
    end
  end

  describe '#added_label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of the issue label names' do
        expect(subject.added_label_names).to eq(['Platform', 'backstage'])
      end
    end

    context 'when the event is an issue update' do
      let(:fixture_path) { 'update_issue.json' }

      it 'returns an array of added label names' do
        expect(subject.added_label_names).to eq(['Platform'])
      end
    end

    context 'whith no label changes' do
      let(:fixture_path) { 'note_on_issue.json' }

      it 'returns an empty array' do
        expect(subject.added_label_names).to eq([])
      end
    end
  end

  describe '#label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of label names' do
        expect(subject.label_names).to contain_exactly('Platform', 'backstage')
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns an array of label names' do
        expect(subject.label_names).to contain_exactly('Platform', 'backstage')
      end
    end

    context 'when field labels is absent' do
      let(:fixture_path) { 'new_issue.json' }

      before do
        event.delete('labels')
      end

      it 'returns an empty array' do
        expect(subject.label_names).to eq([])
      end
    end
  end

  describe '#from_gitlab_org?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project path begins with gitlab-org/' do
      before do
        event['project']['path_with_namespace'] = 'gitlab-org/gitlab'
      end

      it 'returns true' do
        expect(subject.from_gitlab_org?).to eq(true)
      end
    end

    context 'when project path contains but not begins with gitlab-org' do
      before do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-org/gitlab'
      end

      it 'returns false' do
        expect(subject.from_gitlab_org?).to eq(false)
      end
    end
  end

  describe '#from_gitlab_com?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project path begins with gitlab-com/' do
      before do
        event['project']['path_with_namespace'] = 'gitlab-com/gitlab'
      end

      it 'returns true' do
        expect(subject.from_gitlab_com?).to eq(true)
      end
    end

    context 'when project path contains but not begins with gitlab-com' do
      before do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-com/gitlab'
      end

      it 'returns false' do
        expect(subject.from_gitlab_com?).to eq(false)
      end
    end
  end

  describe '#from_eng_prod_team?' do
    let(:fixture_path) { 'new_issue.json' }

    %w[rymai].each do |username|
      context "when username is '#{username}'" do
        before do
          event['user']['username'] = username
        end

        it 'returns true' do
          expect(subject.from_eng_prod_team?).to eq(true)
        end
      end
    end

    context "when username is 'stanhu'" do
      before do
        event['user']['username'] = 'stanhu'
      end

      it 'returns false' do
        expect(subject.from_eng_prod_team?).to eq(false)
      end
    end
  end

  describe '#wider_community_author?', :clean_cache do
    let(:fixture_path) { 'new_issue.json' }

    context "when author is a member of the gitlab-org group" do
      it 'returns false' do
        expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'root' }]) do
          expect(subject.wider_community_author?).to eq(false)
        end
      end
    end

    context "when author is not a member of the gitlab-org group" do
      it 'returns true' do
        expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'another-user' }]) do
          expect(subject.wider_community_author?).to eq(true)
        end
      end
    end
  end

  describe '#wider_gitlab_com_community_author?', :clean_cache do
    let(:fixture_path) { 'new_issue.json' }

    context "when author is a member of the gitlab-com group" do
      it 'returns false' do
        expect_api_request(path: "/groups/#{Triage::GITLAB_COM_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'root' }]) do
          expect(subject.wider_gitlab_com_community_author?).to eq(false)
        end
      end
    end

    context "when author is not a member of the gitlab-com group" do
      it 'returns true' do
        expect_api_request(path: "/groups/#{Triage::GITLAB_COM_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'another-user' }]) do
          expect(subject.wider_gitlab_com_community_author?).to eq(true)
        end
      end
    end
  end

  describe '#with_project_id?' do
    let(:fixture_path) { 'note_on_issue.json' }
    let(:project_id) { 1234 }

    context 'when project id matches' do
      before do
        event['project']['id'] = project_id
      end

      it 'returns true' do
        expect(subject.with_project_id?(project_id)).to eq(true)
      end
    end

    context 'when project id does not match' do
      before do
        event['project']['id'] = 9999
      end

      it 'returns false' do
        expect(subject.with_project_id?(project_id)).to eq(false)
      end
    end
  end

  describe Triage::IssueEvent do
    let(:fixture_path) { 'new_issue.json' }

    describe '#new_comment' do
      it 'returns the issue description' do
        expect(subject.new_comment)
          .to eq('Create new API for manipulations with repository')
      end

      context 'when the description is absent' do
        let(:fixture_path) { 'empty_new_issue.json' }

        it 'returns an empty string' do
          expect(subject.new_comment).to eq('')
        end
      end
    end

    describe '#noteable_path' do
      it 'returns the API URL for the issue' do
        expect(subject.noteable_path)
          .to eq('/projects/1/issues/23')
      end
    end
  end

  describe Triage::MergeRequestEvent do
    let(:fixture_path) { 'new_merge_request.json' }

    describe '#new_comment' do
      it 'returns the merge request description' do
        expect(subject.new_comment).to eq('This is a new merge request')
      end
    end

    describe '#noteable_path' do
      it 'returns the API URL for the merge request' do
        expect(subject.noteable_path)
          .to eq('/projects/1/merge_requests/1')
      end
    end
  end

  describe Triage::NoteEvent do
    context 'when the note is on an issue' do
      let(:fixture_path) { 'note_on_issue.json' }

      describe '#new_comment' do
        it 'returns the new note content' do
          expect(subject.new_comment).to eq('Hello world')
        end
      end

      describe '#noteable_author_id' do
        it 'returns the noteable author id' do
          expect(subject.noteable_author_id)
            .to eq(1)
        end
      end

      describe '#noteable_path' do
        it 'returns the API URL for the issue' do
          expect(subject.noteable_path)
            .to eq('/projects/5/issues/17')
        end
      end
    end

    context 'when the note is on a merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      describe '#new_comment' do
        it 'returns the new note content' do
          expect(subject.new_comment).to eq('This MR needs work.')
        end
      end

      describe '#noteable_path' do
        it 'returns the API URL for the merge request' do
          expect(subject.noteable_path)
            .to eq('/projects/5/merge_requests/1')
        end
      end
    end
  end
end
