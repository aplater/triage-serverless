require 'spec_helper'

require_relative '../triage/triage'

RSpec.describe Triage do
  describe '.run!' do
    let!(:handler) { Triage::Handler.new(context, event) }
    let(:context) { {'HTTP_X_GITLAB_TOKEN' => webhook_token} }
    let(:object_kind) { 'issue' }
    let(:event) { { 'object_kind' => object_kind } }

    before do
      allow(Triage).to receive(:puts)
      allow(Triage::Handler).to receive(:new).and_return(handler)
      allow(handler).to receive(:authenticated?).and_return(authenticated)
    end

    context 'when not authenticated' do
      let(:authenticated) { false }

      it 'returns { status: :unauthenticated }' do
        result = Triage.run!(context: context, event: event)

        expect(result).to eq(status: :unauthenticated)
      end

      context 'with empty event' do
        let(:event) { {} }

        it 'returns { status: :unauthenticated }' do
          result = Triage.run!(context: context, event: event)

          expect(result).to eq(status: :unauthenticated)
        end
      end
    end

    context 'when authenticated' do
      let(:authenticated) { true }

      it 'calls handler.process and returns status and message' do
        messages = %w[message1 message2]
        expect(handler).to receive(:process).and_return(messages)

        result = described_class.run!(context: context, event: event)

        expect(result).to eq(status: :ok, messages: messages)
      end

      context 'with empty event' do
        let(:event) { {} }

        it 'returns an error with Triage::Event::ObjectKindNotProvidedError' do
          result = described_class.run!(context: context, event: event)

          expect(result).to include(status: :error, error: Triage::Event::ObjectKindNotProvidedError, message: a_kind_of(String))
        end
      end

      context 'with runtime error' do
        let(:error) { StandardError.new('runtime error') }

        before do
          allow(handler).to receive(:process).and_raise(error)
        end

        it 'captures exception into Sentry' do
          expect(Raven).to receive(:capture_exception).with(error)

          Triage.run!(context: context, event: event)
        end

        it 'tags Sentry error with event type' do
          expect(Raven).to receive(:tags_context).with(object_kind: object_kind)

          Triage.run!(context: context, event: event)
        end

        it 'adds event into Sentry as extra context' do
          expect(Raven).to receive(:extra_context).with(event: event)

          Triage.run!(context: context, event: event)
        end
      end

      describe '.gitlab_org_group_member_usernames', :clean_cache do
        it 'caches the API response' do
          expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'root' }]) do
            # Populate the cache
            expect(described_class.gitlab_org_group_member_usernames).to eq(['root'])

          end

          # Next call is retrieved from cache
          expect(described_class.gitlab_org_group_member_usernames).to eq(['root'])

          # Travel 1 second after the cache expires
          Timecop.travel(Time.now + described_class::GITLAB_ORG_GROUP_MEMBERS_CACHE_EXPIRATION + 1)

          expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'another-user' }]) do
            # Ensure the response is up-to-date
            expect(described_class.gitlab_org_group_member_usernames).to eq(['another-user'])
          end

          # Next call is retrieved from cache
          expect(described_class.gitlab_org_group_member_usernames).to eq(['another-user'])
        end

        context 'when `fresh: true` is passed' do
          it 'calls the API and saves the response in the cache' do
            expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'root' }]) do
              # Populate the cache
              expect(described_class.gitlab_org_group_member_usernames).to eq(['root'])
            end

            expect_api_request(path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'another-user' }]) do
              # Ensure the response is up-to-date
              expect(described_class.gitlab_org_group_member_usernames(fresh: true)).to eq(['another-user'])
            end

            # Next call without `fresh: true` is retrieved from cache
            expect(described_class.gitlab_org_group_member_usernames).to eq(['another-user'])
          end
        end
      end

      describe '.gitlab_com_group_member_usernames', :clean_cache do
        it 'caches the API response' do
          expect_api_request(path: "/groups/#{Triage::GITLAB_COM_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'root' }]) do
            # Populate the cache
            expect(described_class.gitlab_com_group_member_usernames).to eq(['root'])

          end

          # Next call is retrieved from cache
          expect(described_class.gitlab_com_group_member_usernames).to eq(['root'])

          # Travel 1 second after the cache expires
          Timecop.travel(Time.now + described_class::GITLAB_COM_GROUP_MEMBERS_CACHE_EXPIRATION + 1)

          expect_api_request(path: "/groups/#{Triage::GITLAB_COM_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'another-user' }]) do
            # Ensure the response is up-to-date
            expect(described_class.gitlab_com_group_member_usernames).to eq(['another-user'])
          end

          # Next call is retrieved from cache
          expect(described_class.gitlab_com_group_member_usernames).to eq(['another-user'])
        end

        context 'when `fresh: true` is passed' do
          it 'calls the API and saves the response in the cache' do
            expect_api_request(path: "/groups/#{Triage::GITLAB_COM_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'root' }]) do
              # Populate the cache
              expect(described_class.gitlab_com_group_member_usernames).to eq(['root'])
            end

            expect_api_request(path: "/groups/#{Triage::GITLAB_COM_GROUP}/members", query: { per_page: 100 }, response_body: [{ username: 'another-user' }]) do
              # Ensure the response is up-to-date
              expect(described_class.gitlab_com_group_member_usernames(fresh: true)).to eq(['another-user'])
            end

            # Next call without `fresh: true` is retrieved from cache
            expect(described_class.gitlab_com_group_member_usernames).to eq(['another-user'])
          end
        end
      end
    end
  end
end
