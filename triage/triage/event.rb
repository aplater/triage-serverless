# frozen_string_literal: true

require 'gitlab'

require_relative 'error'

module Triage
  class Event
    ObjectKindNotProvidedError = Class.new(Triage::ClientError)

    ENG_PROD_TEAM_USERNAMES = %w[
      caalberts
      godfat-gitlab
      kwiebers
      markglenfletcher
      rymai
    ].freeze

    attr_reader :event

    def initialize(event)
      @event = event
    end

    def self.build(event)
      event['object_kind'].yield_self do |kind|
        if kind.nil?
          raise ObjectKindNotProvidedError, '`object_kind` not provided!'
        end

        case kind
        when 'issue'
          IssueEvent.new(event)
        when 'merge_request'
          MergeRequestEvent.new(event)
        when 'note'
          NoteEvent.new(event)
        else
          raise "Unknown object: #{kind.inspect}"
        end
      end
    end

    def issue?
      object_kind == 'issue'
    end

    def merge_request?
      object_kind == 'merge_request'
    end

    def note?
      object_kind == 'note'
    end

    def user
      event.fetch('user', {})
    end

    def author_id
      event.dig('object_attributes', 'author_id')
    end

    def new_comment
      event.dig('object_attributes', 'description').to_s
    end

    def new_entity?
      event.dig('object_attributes', 'action') == 'open'
    end

    def added_label_names
      current_label_names - previous_label_names
    end

    def label_names
      labels.map { |label_data| label_data['title'] }
    end

    def noteable_path
      "/projects/#{project_id}/#{noteable_kind}/#{noteable_iid}"
    end

    def from_gitlab_org?
      event_from_group?(Triage::GITLAB_ORG_GROUP)
    end

    def from_gitlab_com?
      event_from_group?(Triage::GITLAB_COM_GROUP)
    end

    def from_eng_prod_team?
      ENG_PROD_TEAM_USERNAMES.include?(user['username'])
    end

    def wider_community_author?
      !Triage.gitlab_org_group_member_usernames.include?(user['username'])
    end

    def wider_gitlab_com_community_author?
      !Triage.gitlab_com_group_member_usernames.include?(user['username'])
    end

    def with_project_id?(expected_project_id)
      event.dig('project', 'id') == expected_project_id
    end

    private

    def project_id
      event.dig('project', 'id')
    end

    def object_kind
      event.fetch('object_kind', 'unknown')
    end

    def noteable_kind
      raise NotImplementedError
    end

    def noteable_iid
      event.dig('object_attributes', 'iid')
    end

    def changes
      event.fetch('changes', {})
    end

    def labels
      event.fetch('labels', [])
    end

    def previous_labels
      changes.dig('labels', 'previous') || []
    end

    def current_labels
      if new_entity?
        labels
      else
        changes.dig('labels', 'current') || []
      end
    end

    def previous_label_names
      previous_labels.map { |l| l['title'] }
    end

    def current_label_names
      current_labels.map { |l| l['title'] }
    end

    def event_from_group?(group)
      %r{\A#{group}/}.match?(event.dig('project', 'path_with_namespace'))
    end
  end

  class IssueEvent < Event
    private

    def noteable_kind
      'issues'
    end
  end

  class MergeRequestEvent < Event
    private

    def noteable_kind
      'merge_requests'
    end
  end

  class NoteEvent < Event
    def new_comment
      event.dig('object_attributes', 'note').to_s
    end

    def noteable_author_id
      event.dig(noteable_type, 'author_id')
    end

    private

    def noteable_type
      to_snake_case(event.dig('object_attributes', 'noteable_type'))
    end

    def noteable_kind
      "#{noteable_type}s"
    end

    def noteable_iid
      event.dig(noteable_type, 'iid')
    end

    def to_snake_case(string)
      string.gsub(/(?<=[a-z])[A-Z]/, '_\0').downcase
    end
  end
end
