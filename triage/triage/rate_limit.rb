# frozen_string_literal: true

module Triage
  module RateLimit
    RATE_LIMIT = 60
    RATE_LIMIT_PERIOD = 3600 # 1 hour

    def self.included(base)
      base.prepend(Hooks)
    end

    private

    def rate_limited?
      Triage.cache.get(cache_key).to_i >= RATE_LIMIT
    end

    def increment_commands_sent
      new_counter_value, expires_in = if Triage.cache.set?(cache_key)
        current_counter_data = Triage.cache.data[cache_key]
        [current_counter_data.value + 1, (current_counter_data.expires_in - Time.now).to_i]
      else
        [1, RATE_LIMIT_PERIOD]
      end

      Triage.cache.set(cache_key, new_counter_value, expires_in: expires_in)
    end

    def cache_key
      raise NotImplementedError
    end

    module Hooks
      def applicable?
        return false if rate_limited?

        super
      end

      def before_process
        increment_commands_sent

        super
      end
    end
  end
end
