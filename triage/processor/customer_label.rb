# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class CustomerLabel < Triager
    def process
      add_customer_label if has_customer_link?
    end

    private

    def has_customer_link?(new_comment = event.new_comment)
      new_comment.include?('gitlab.zendesk.com') ||
        new_comment.include?('gitlab.my.salesforce.com')
    end

    def add_customer_label
      add_comment('/label ~customer')
    end
  end
end
