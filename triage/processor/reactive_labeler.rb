# frozen_string_literal: true

require_relative '../triage/triager'
require_relative '../triage/rate_limit'

require 'digest'

module Triage
  class ReactiveLabeler < Triager
    include RateLimit

    GITLAB_BOT = '@gitlab-bot'
    ACTION_PREFIX_REGEXP = /^#{Regexp.escape(GITLAB_BOT)}[[:space:]]+label[[:space:]]+/
    ACTION_REGEXP = /#{ACTION_PREFIX_REGEXP}.+/
    LABELS_REGEX = /~"([^"]+)"|~([^ ]+)/.freeze
    ALLOWED_LABELS_REGEX = /\Agroup::[^:]+\z/.freeze

    def applicable?
      event.from_gitlab_org? &&  # Check this first for code clarity
        event_by_noteable_author? && # Fast and won't pass for most of time
        valid_action_prefix? &&
        any_labels_to_apply?
    end

    def process
      post_label_command
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("reactive-labeler-commands-sent-#{event.user['id']}")
    end

    private

    def actions
      @actions ||= event.new_comment.scan(ACTION_REGEXP)
    end

    def valid_action_prefix?
      actions.any?
    end

    def any_labels_to_apply?
      labels_to_apply.any?
    end

    def event_by_noteable_author?
      # A new issue/MR is always created by its author
      return true if event.new_entity?

      event.note? && event.user['id'] == event.noteable_author_id
    end

    def labels_to_apply
      @labels_to_apply ||= actions
        .flat_map { |line| line.sub(ACTION_PREFIX_REGEXP, '').scan(LABELS_REGEX) }
        .flatten
        .compact
        .select { |label| label.match?(ALLOWED_LABELS_REGEX) }
    end

    def command_labels
      labels_to_apply.map { |label| %Q(~"#{label}") }.join(' ')
    end

    def post_label_command
      add_comment <<~MARKDOWN.chomp
        /label #{command_labels}
      MARKDOWN
    end
  end
end
