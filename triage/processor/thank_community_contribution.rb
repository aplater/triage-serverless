# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class ThankCommunityContribution < Triager
    def process
      return unless event.from_gitlab_org?
      return unless event.merge_request?
      return unless event.new_entity?
      return unless event.wider_community_author?

      post_thank_you_message
    end

    private

    def post_thank_you_message
      add_comment <<~MARKDOWN.chomp
        :wave: @#{event.user['username']}

        Thank you for your contributions to GitLab. We believe that [everyone can contribute](https://about.gitlab.com/company/strategy/#everyone-can-contribute) and contributions like yours are what make GitLab great!

        Our [Merge Request coaches](https://about.gitlab.com/company/team/?department=merge-request-coach) will ensure your contribution is reviewed in a timely manner.

        If after a few days, there's no message from a GitLab team member, feel free to ping `@gitlab-org/coaches`.

        These resources may help you to move your Merge Request to the next steps:

        - [Contributing to GitLab](https://about.gitlab.com/community/contribute/)
        - [How to get help](https://about.gitlab.com/community/contribute/#getting-help)
        - [Ping a Merge request coach](https://about.gitlab.com/company/team/?department=merge-request-coach)

        /label ~"Community contribution"
      MARKDOWN
    end
  end
end
