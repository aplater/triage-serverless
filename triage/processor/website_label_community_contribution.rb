# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class WebsiteLabelCommunityContribution < Triager
    WWW_GITLAB_COM_PROJECT_ID = 7764

    def process
      return unless event.from_gitlab_com?
      return unless event.merge_request?
      return unless event.new_entity?
      return unless event.with_project_id?(WWW_GITLAB_COM_PROJECT_ID)
      return unless event.wider_gitlab_com_community_author?

      post_thank_you_message
    end

    private

    def post_thank_you_message
      add_comment <<~MARKDOWN.chomp
        Hi @#{event.user['username']},

        Thanks so much for your contribution to the GitLab website! :heart:

        I'll notify the Website team about your Merge Request and they will get back to you as soon as they can.
        If you don't hear from someone in a reasonable amount of time, please ping us again in a comment and mention @gl-website.

        /label ~"Community contribution"
      MARKDOWN
    end
  end
end
