# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class BackstageLabel < Triager
    BACKSTAGE_LABELS = [
      'backstage',
      'backstage [DEPRECATED]'
    ].freeze

    def process
      return unless event.from_gitlab_org?
      return unless backstage_label_added?

      post_deprecation_message
    end

    private

    def backstage_label_added?
      !!backstage_label_added
    end

    def backstage_label_added
      (event.added_label_names & BACKSTAGE_LABELS).first
    end

    def post_deprecation_message
      add_comment <<~MARKDOWN.chomp
        Hey @#{event.user['username']}, ~"#{backstage_label_added}" is being deprecated in favor of ~"feature::addition", ~"feature::maintenance", ~"tooling::pipelines", and ~"tooling::workflow" to improve the identification of these type of changes.
        Please see https://about.gitlab.com/handbook/engineering/management/throughput/#implementation for further guidance.
        /unlabel ~"#{backstage_label_added}"
      MARKDOWN
    end
  end
end
